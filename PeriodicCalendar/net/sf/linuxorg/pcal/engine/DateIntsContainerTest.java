/**
 *  Copyright (C) 2009 by Mar'yan Rachynskyy
 *  mrach@users.sourceforge.net
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.sf.linuxorg.pcal.engine;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Mar'yan Rachynskyy
 *
 */
public class DateIntsContainerTest {

	/**
	 * Test method for {@link net.sf.linuxorg.pcal.engine.DateIntsContainer#DateIntsContainer()}.
	 */
	@Test
	public final void testDateIntsContainer() {
		DateIntsContainer intsContainer = new DateIntsContainer();
		for(int i=0; i<DateIntsContainer.getIntsCount(); i++) {
			assertNull(intsContainer.getIntValue(i));
		}
	}

	/**
	 * Test method for {@link net.sf.linuxorg.pcal.engine.DateIntsContainer#DateIntsContainer(int, java.lang.Integer)}.
	 */
	@Test
	public final void testDateIntsContainerIntInteger() {
		for(int i = 0; i<DateIntsContainer.getIntsCount(); i++) {
			DateIntsContainer intsContainer = new DateIntsContainer(i, i+5);
			for(int j = 0; j<DateIntsContainer.getIntsCount(); j++) {
				if(i==j) {
					assertTrue(intsContainer.getIntValue(j) == i+5);
				} else {
					assertNull(intsContainer.getIntValue(j));
				}
			}
		}
	}

	/**
	 * Test method for {@link net.sf.linuxorg.pcal.engine.DateIntsContainer#getIntValue(int)}.
	 */
	@Test
	public final void testGetIntValue() {
		DateIntsContainer intsContainer = new DateIntsContainer();
		
		intsContainer.setIntValue(0, 3);
		assertTrue(intsContainer.getIntValue(0) == 3);
		
		intsContainer.setIntValue(100, 1);
		assertEquals(intsContainer.getIntValue(3), null);
		assertEquals(intsContainer.getIntValue(100), null);
		
		intsContainer.setIntValue(0, null);
		assertEquals(intsContainer.getIntValue(0), null);
	}

	/**
	 * Test method for {@link net.sf.linuxorg.pcal.engine.DateIntsContainer#toString()}.
	 */
	@Test
	public final void testToString() {
		DateIntsContainer intsContainer = new DateIntsContainer();
		assertEquals(intsContainer.toString(), ",,,"); //$NON-NLS-1$
		
		intsContainer.setIntValue(0, 3);
		assertEquals(intsContainer.toString(), "3,,,"); //$NON-NLS-1$
		
		intsContainer.setIntValue(3, 2);
		assertEquals(intsContainer.toString(), "3,,,2"); //$NON-NLS-1$
	}

	/**
	 * Test method for {@link net.sf.linuxorg.pcal.engine.DateIntsContainer#parseFromString(java.lang.String)}.
	 */
	@Test
	public final void testParseFromString() {
		String testStrings[] = {",,,", ",1,2,3", "1,2,3,", "1,2,3,4", "1,2,3", "1,2,", "1,2"}; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
		Integer expectedInts[][] = {
				{null, null, null, null},
				{null, 1, 2, 3},
				{1, 2, 3, null},
				{1, 2, 3, 4},
				{1, 2, 3, null},
				{1, 2, null, null},
				{1, 2, null, null}
		};
		
		
		
		for(int i = 0; i< testStrings.length; i++) {
			DateIntsContainer intsContainer = DateIntsContainer.parseFromStringFactory(testStrings[i]);
			for(int j = 0; j< expectedInts[i].length; j++) {
				assertEquals(intsContainer.getIntValue(j), expectedInts[i][j]);
			}
		}
	}

	/**
	 * Test method for {@link net.sf.linuxorg.pcal.engine.DateIntsContainer#getIntsCount()}.
	 */
	@Test
	public final void testGetIntsCount() {
		assertEquals(DateIntsContainer.getIntsCount(), 4);
	}

}
