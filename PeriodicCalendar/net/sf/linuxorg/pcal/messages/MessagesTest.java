package net.sf.linuxorg.pcal.messages;

import static org.junit.Assert.*;

import org.junit.Test;

public class MessagesTest {

	@Test
	public final void testGetString() {
		assertEquals(Messages.getString("Messages.TEST"), "TEST"); //$NON-NLS-1$ //$NON-NLS-2$
		assertEquals(Messages.getString("IMPOSSIBLE_KEY"), "!IMPOSSIBLE_KEY!"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@Test
	public final void testGetMnemonic() {
		assertEquals(Messages.getMnemonic("Messages.TEST.Mnemonic"), 77); //$NON-NLS-1$
		assertEquals(Messages.getMnemonic("IMPOSSIBLE_KEY"), 0); //$NON-NLS-1$
	}

}
